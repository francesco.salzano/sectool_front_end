import { TestBed } from '@angular/core/testing';

import { SectoolService } from './sectool.service';

describe('SectoolService', () => {
  let service: SectoolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SectoolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
