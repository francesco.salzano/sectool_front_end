import {Component, OnInit} from '@angular/core';
import {SectoolService} from '../sectool.service';

@Component({
  selector: 'app-pw',
  templateUrl: './pw.component.html',
  styleUrls: ['./pw.component.css']
})
export class PwComponent implements OnInit {
  public hash: string = '';
  numberOfThreads: number = 0;
  maxPlaintextLength: number = 0;
  public hashType: string = '';
  public endPoint: string = '';

  constructor(public sectoolService: SectoolService) {
  }

  ngOnInit(): void {
  }

  cancelPw() {
    console.log(this.hash);
    this.hash = '';
    console.log(this.hash);

  }


  breakHash() {
    this.sectoolService.hashSuccess = false;
    this.sectoolService.breakHash(this.hash, this.maxPlaintextLength, this.numberOfThreads, this.hashType);
  }


}
