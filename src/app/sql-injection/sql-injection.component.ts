import { Component, OnInit } from '@angular/core';
import {SectoolService} from '../sectool.service';

@Component({
  selector: 'app-sql-injection',
  templateUrl: './sql-injection.component.html',
  styleUrls: ['./sql-injection.component.css']
})
export class SqlInjectionComponent implements OnInit {

  constructor(public sectoolService: SectoolService) { }
  public getAdminTableHeaders: string[] = ['Unit', 'Name'];



  ngOnInit(): void {
  }

  getCredentials() {
    this.sectoolService.getCredentials();
  }

  getAdminList() {
    this.sectoolService.getAdminList();
  }
}
