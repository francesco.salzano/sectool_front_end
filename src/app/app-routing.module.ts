import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {SqlInjectionComponent} from './sql-injection/sql-injection.component';
import {PwComponent} from './pw/pw.component';

const appRoutes: Routes = [
  { path: 'sql', component: SqlInjectionComponent },
  { path: 'pw', component: PwComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
